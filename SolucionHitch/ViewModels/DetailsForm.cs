﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolucionHitch.ViewModels
{
    public class DetailsForm
    {
        private string id;
        private string sku;
        private string name;
        private string price;
        private string category;
        private DateTime? createDate;
        private string type;
        private string upc;
        private string shipping;
        private string manufacturer;
        private string model;
        private string description;
        private DateTime? updateDate;
        private string url;

        private string imagepath;

        public string Id { get => id; set => id = value; }
        public string Sku { get => sku; set => sku = value; }
        public string Name { get => name; set => name = value; }
        public string Price { get => price; set => price = value; }
        public string Category { get => category; set => category = value; }
        public DateTime? CreateDate { get => createDate; set => createDate = value; }
        public string Type { get => type; set => type = value; }
        public string Upc { get => upc; set => upc = value; }
        public string Shipping { get => shipping; set => shipping = value; }
        public string Manufacturer { get => manufacturer; set => manufacturer = value; }
        public string Model { get => model; set => model = value; }
        public string Description { get => description; set => description = value; }
        public DateTime? UpdateDate { get => updateDate; set => updateDate = value; }
        public string Url { get => url; set => url = value; }

        public string Imagepath { get => imagepath; set => imagepath = value; }
    }
}
