﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SolucionHitch.Entities;
using SolucionHitch.ViewModels;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace SolucionHitch
{
    public partial class Form1 : Form
    {

        private void Form1_Load(object sender, EventArgs e)
        {
            GridPopulate();

        }

        public Form1()
        {
            InitializeComponent();
            GridPopulate();
            GridApiPopulate();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var gridSender = (DataGridView)sender;
            if (gridSender.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                DetailsForm detalles = new DetailsForm();

                detalles.Id = gridSender.CurrentRow.Cells["Id"].Value.ToString();
                detalles.Sku = gridSender.CurrentRow.Cells["SKU"].Value.ToString();
                detalles.Type = gridSender.CurrentRow.Cells["Type"].Value.ToString();
                detalles.Price = gridSender.CurrentRow.Cells["Price"].Value.ToString();
                detalles.Category = gridSender.CurrentRow.Cells["Category"].Value.ToString();
                detalles.CreateDate = (DateTime)gridSender.CurrentRow.Cells["CreateDate"].Value;
                detalles.Upc = gridSender.CurrentRow.Cells["Upc"].Value.ToString();
                detalles.Shipping = gridSender.CurrentRow.Cells["Shipping"].Value.ToString();
                detalles.Manufacturer = gridSender.CurrentRow.Cells["Manufacturer"].Value.ToString();
                detalles.Model = gridSender.CurrentRow.Cells["Model"].Value.ToString();
                detalles.Description = gridSender.CurrentRow.Cells["Description"].Value.ToString();
                detalles.Url = gridSender.CurrentRow.Cells["Url"].Value.ToString();
                detalles.UpdateDate = (DateTime)gridSender.CurrentRow.Cells["updateDate"].Value;
                detalles.Imagepath = gridSender.CurrentRow.Cells["Imagepath"].Value.ToString();



                Form2 newForm = new Form2();
                newForm.txtId.Text = detalles.Id;
                newForm.txtSku.Text = detalles.Sku;
                newForm.txtType.Text = detalles.Type;
                newForm.txtPrice.Text = detalles.Price.ToString();
                newForm.txtUpc.Text = detalles.Upc;
                newForm.txtShipping.Text = detalles.Shipping;
                newForm.txtManufacturer.Text = detalles.Manufacturer;
                newForm.txtModel.Text = detalles.Model;
                newForm.txtDescription.Text = detalles.Description;
                newForm.txtCategory.Text = detalles.Category;
                newForm.txtCreateDate.Text = detalles.CreateDate.ToString();
                newForm.txtUpdateDate.Text = detalles.UpdateDate.ToString();
                newForm.lnkUrl.Text = detalles.Url;

                if (String.IsNullOrEmpty(detalles.Imagepath))
                {
                    newForm.imgImagen.Visible = false;
                }
                else
                {
                    newForm.imgImagen.Load(detalles.Imagepath);
                }
                newForm.imgImagen.SizeMode = PictureBoxSizeMode.Zoom;
                newForm.Show(this);
            }
        }

        public void GridPopulate()
        {
            using (DataHitchEntities entities = new DataHitchEntities())
            {
                List<product> productos = entities.products.ToList();
                List<ProductGrid> listaProductos = new List<ProductGrid>();

                foreach (var producto in productos)
                {
                    ProductGrid nuevoItem = new ProductGrid();
                    nuevoItem.Id = producto.id;
                    nuevoItem.Sku = producto.SKU;
                    nuevoItem.Name = producto.name;
                    nuevoItem.Price = producto.price.ToString();
                    nuevoItem.Category = producto.category.name;
                    nuevoItem.CreateDate = producto.create_time;
                    nuevoItem.Type = producto.type;
                    nuevoItem.Upc = producto.upc;
                    nuevoItem.Shipping = producto.shipping.ToString();
                    nuevoItem.Manufacturer = producto.manufacturer;
                    nuevoItem.Model = producto.model;
                    nuevoItem.Description = producto.description;
                    nuevoItem.Url = producto.url;
                    nuevoItem.UpdateDate = producto.update_time;
                    nuevoItem.Imagepath = producto.image;

                    listaProductos.Add(nuevoItem);
                }

                dataGridView1.DataSource = listaProductos;

                // Reemplazo nombre de cabeceras; solución poco elegante
                dataGridView1.Columns[0].HeaderText = "Id";
                dataGridView1.Columns[1].HeaderText = "SKU";
                dataGridView1.Columns[2].HeaderText = "Nombre";
                dataGridView1.Columns[3].HeaderText = "Precio";
                dataGridView1.Columns[4].HeaderText = "Categoría";
                dataGridView1.Columns[5].HeaderText = "Fecha Creación";
                dataGridView1.Columns["Type"].Visible = false;
                dataGridView1.Columns["Upc"].Visible = false;
                dataGridView1.Columns["Shipping"].Visible = false;
                dataGridView1.Columns["Manufacturer"].Visible = false;
                dataGridView1.Columns["Model"].Visible = false;
                dataGridView1.Columns["Description"].Visible = false;
                dataGridView1.Columns["Url"].Visible = false;
                dataGridView1.Columns["updateDate"].Visible = false;
                //dataGridView1.Columns["ImageBinary"].Visible = false;
                dataGridView1.Columns["Imagepath"].Visible = false;

                // Botón para mostrar detalles
                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                dataGridView1.Columns.Add(btn);
                btn.HeaderText = "Acciones";
                btn.Text = "Detalle";
                btn.Name = "btnDetails";
                btn.UseColumnTextForButtonValue = true;

            }

        }

        public async void GridApiPopulate()
        {
            string urlBase = "http://143.198.76.43/wms/api/v1/products";
            List<ProductGrid> listaGrid = new List<ProductGrid>();
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            try
            {
                using (HttpClient cliente = new HttpClient())
                {
                    using (HttpResponseMessage respuesta = await cliente.GetAsync(urlBase))
                    {
                        using (HttpContent content = respuesta.Content)
                        {
                            var data = await content.ReadAsStringAsync();
                            if (data != null)
                            {
                                var jArray = JArray.Parse(data);
                                foreach (var item in jArray)
                                {
                                    ProductGrid productoGrid = new ProductGrid();
                                    productoGrid.Id = (int)item["id"];
                                    productoGrid.Sku = item["sku"].ToString();
                                    productoGrid.Name = item["name"].ToString();
                                    productoGrid.Price = item["price"].ToString();
                                    productoGrid.Type = item["type"].ToString();
                                    productoGrid.Upc = item["upc"].ToString();
                                    productoGrid.Shipping = item["shipping"].ToString();
                                    productoGrid.Manufacturer = item["manufacturer"].ToString();
                                    productoGrid.Description = item["description"].ToString();
                                    productoGrid.Model = item["model"].ToString();
                                    productoGrid.Url = item["url"].ToString();
                                    productoGrid.Category = item["category"].ToString();

                                    // productoGrid.Category = item[]["category"].Value<string>("name");
                                    //string distance = item.SelectToken("category[0].name[0]").ToString();

                                    //if (item["category"].HasValues)
                                    //{
                                    //    string distance = item.SelectToken("category[0].name[0]").ToString();
                                    //}
                                    //else
                                    //{
                                    //    productoGrid.Category = item["category"].ToString();
                                    //}

                                    productoGrid.Imagepath = item["image"].ToString();

                                    if (item["createdate"] == null)
                                    {
                                        productoGrid.CreateDate = DateTime.Parse("1/1/01");
                                    }

                                    if (item["updatedate"] == null)
                                    {
                                        productoGrid.UpdateDate = DateTime.Parse("1/1/01");
                                    }
                                    listaGrid.Add(productoGrid);

                                }

                                dataGridView2.DataSource = listaGrid;

                                dataGridView2.Columns[0].HeaderText = "Id";
                                dataGridView2.Columns[1].HeaderText = "SKU";
                                dataGridView2.Columns[2].HeaderText = "Nombre";
                                dataGridView2.Columns[3].HeaderText = "Precio";
                                dataGridView2.Columns[4].HeaderText = "Categoría";
                                dataGridView2.Columns["Type"].Visible = false;
                                dataGridView2.Columns["Upc"].Visible = false;
                                dataGridView2.Columns["Shipping"].Visible = false;
                                dataGridView2.Columns["Manufacturer"].Visible = false;
                                dataGridView2.Columns["Model"].Visible = false;
                                dataGridView2.Columns["Description"].Visible = false;
                                dataGridView2.Columns["Url"].Visible = false;
                                dataGridView2.Columns["updateDate"].Visible = false;
                                dataGridView2.Columns["Imagepath"].Visible = false;
                                //    dataGridView2.Columns["ImageBinary"].Visible = false;

                                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                                dataGridView2.Columns.Add(btn);
                                btn.HeaderText = "Acciones";
                                btn.Text = "Detalle";
                                btn.Name = "btnDetails";
                                btn.UseColumnTextForButtonValue = true;
                            }
                            else
                            {
                                Console.WriteLine("No hay datos");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: ", ex.Message);
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var gridSender = (DataGridView)sender;
            if (gridSender.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                DetailsForm detalles = new DetailsForm();

                detalles.Id = gridSender.CurrentRow.Cells["Id"].Value.ToString();
                detalles.Sku = gridSender.CurrentRow.Cells["SKU"].Value.ToString();
                detalles.Type = gridSender.CurrentRow.Cells["Type"].Value.ToString();
                detalles.Price = gridSender.CurrentRow.Cells["Price"].Value.ToString();
                detalles.Upc = gridSender.CurrentRow.Cells["Upc"].Value.ToString();
                detalles.Shipping = gridSender.CurrentRow.Cells["Shipping"].Value.ToString();
                detalles.Manufacturer = gridSender.CurrentRow.Cells["Manufacturer"].Value.ToString();
                detalles.Model = gridSender.CurrentRow.Cells["Model"].Value.ToString();
                detalles.Description = gridSender.CurrentRow.Cells["Description"].Value.ToString();
                detalles.Url = gridSender.CurrentRow.Cells["Url"].Value.ToString();

                detalles.Imagepath = gridSender.CurrentRow.Cells["Imagepath"].Value.ToString();

                Form2 newForm = new Form2();
                newForm.txtId.Text = detalles.Id;
                newForm.txtSku.Text = detalles.Sku;
                newForm.txtType.Text = detalles.Type;
                newForm.txtPrice.Text = detalles.Price.ToString();
                newForm.txtUpc.Text = detalles.Upc;
                newForm.txtShipping.Text = detalles.Shipping;
                newForm.txtManufacturer.Text = detalles.Manufacturer;
                newForm.txtModel.Text = detalles.Model;
                newForm.txtDescription.Text = detalles.Description;
                newForm.txtCategory.Text = detalles.Category;
                newForm.txtCreateDate.Text = detalles.CreateDate.ToString();
                newForm.txtUpdateDate.Text = detalles.UpdateDate.ToString();
                newForm.lnkUrl.Text = detalles.Url;
                newForm.imgImagen.SizeMode = PictureBoxSizeMode.Zoom;

                if (String.IsNullOrEmpty(detalles.Imagepath))
                {
                    newForm.imgImagen.Visible = false;
                }
                else
                {
                    newForm.imgImagen.Load(detalles.Imagepath);
                }

                newForm.Show(this);
            }
        }


    }
}
